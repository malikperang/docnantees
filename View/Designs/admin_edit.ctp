<div class="designs form">
<?php echo $this->Form->create('Design'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Design'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('image');
		echo $this->Form->input('challenge_id');
		echo $this->Form->input('title');
		echo $this->Form->input('story');
		echo $this->Form->input('notes');
		echo $this->Form->input('category_id');
		echo $this->Form->input('designs_tags');
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>