<?php echo $this->Html->css('gallery'); ?>
<!--sidebar menu-->
<?php echo $this->element('menu/sidebar'); ?>
<?php $userDetails = $this->Session->read('Auth.User'); ?>
<!--design section-->
<div class="col-xs-12 col-sm-6 col-lg-10">
    <div class="row container">
            <div class="gallery-container">
                <h2><?php echo __('Designs'); ?></h2>

                <ul class="grid cs-style-3">
                <?php foreach ($designs as $design): ?>
                    <?php //debug($design); ?>
                      <li>
                          <figure>
                              <?php echo $this->Html->image($design['Design']['image'], array('width'=>'300'));?>
                              <figcaption>
                                  <h3>Vote for <?php echo $design['Design']['title']; ?></h3>
                                  <div align="center">
                                  <?php 
                                      echo $this->Form->create('Vote',array('action' => 'add'));
                                      echo $this->Form->input('user_id', array('type'=>'hidden', 'value'=>$userDetails['id']));
                                      echo $this->Form->input('design_id', array('type'=>'hidden', 'value'=>$design['Design']['id']));
                                      for($i=1; $i<6; $i++):
                                        echo '<button class="btn btn-round btn-danger" type="submit" name="vote_id"  id="vote_id" value="'.$i.'">'.$i.'</button>';
                                      endfor;
                                      echo $this->Form->end();
                                   ?>
                                   </div>
                              </figcaption>
                          </figure>
                      </li>                      
                 <?php endforeach;?>
                </ul>
            </div>
        </div>
        <div class="row container">
        <?php
        echo $this->Paginator->counter(array(
        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>
        </div>
        <div class="row container">
        <ul class="pagination">
                          <li><?php echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')); ?> </li>
                          <li><?php echo $this->Paginator->numbers(array('separator' => '')); ?> </li>
                          <li><?php echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));?></li>
        </ul>
        </div>
    </div>
</div>
<?php echo $this->Html->script('modernizr.custom.js'); ?>
    
