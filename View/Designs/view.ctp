<?php $userDetails = $this->Session->read('Auth.User'); ?>
<div class="designs view">
<div class="row">
	<?php echo $this->Form->create('Vote',array('action' => 'add'));?>
	<h3><?php echo h($design['Design']['title']); ?></h3>created by
	<?php echo $this->Html->link(__(h($design['User']['name'])), array('controller'=>'users', 'action' => 'view', $design['User']['id'])); ?>
</div>
<div class="row">
	<?php echo $this->Html->image($design['Design']['image'] , array('fullBase' => true , 'height' => 200, 'width' => 670,'class' => 'course-img'));?>
 </div>

<?php 
	  echo $this->Form->input('user_id', array('type'=>'hidden', 'value'=>$userDetails['id']));
	  echo $this->Form->input('design_id', array('type'=>'hidden', 'value'=>$design['Design']['id']));
	  for($i=1; $i<6; $i++):
		echo '<button type="submit"  name="vote_id"  id="vote_id" value="'.$i.'">'.$i.'</button>';
	  endfor;
	  echo $this->Form->end();
?>
<!-- 

</div>
<h2><?php echo __('Design'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($design['Design']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Theme'); ?></dt>
		<dd>
			<?php echo $this->Html->link($design['Challenge']['name'], array('controller' => 'challenges', 'action' => 'view', $design['Challenge']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($design['Design']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Story'); ?></dt>
		<dd>
			<?php echo h($design['Design']['story']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Notes'); ?></dt>
		<dd>
			<?php echo h($design['Design']['notes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($design['Category']['name'], array('controller' => 'categories', 'action' => 'view', $design['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Designs Tags'); ?></dt>
		<dd>
			<?php echo h($design['Design']['designs_tags']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			&nbsp;</p>
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Design'), array('action' => 'edit', $design['Design']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Design'), array('action' => 'delete', $design['Design']['id']), null, __('Are you sure you want to delete # %s?', $design['Design']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Designs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Design'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Themes'), array('controller' => 'themes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Theme'), array('controller' => 'themes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Votes'), array('controller' => 'votes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vote'), array('controller' => 'votes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Votes'); ?></h3>
	<?php if (!empty($design['Vote'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Design Id'); ?></th>
		<th><?php echo __('Rating'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($design['Vote'] as $vote): ?>
		<tr>
			<td><?php echo $vote['id']; ?></td>
			<td><?php echo $vote['user_id']; ?></td>
			<td><?php echo $vote['design_id']; ?></td>
			<td><?php echo $vote['rating']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'votes', 'action' => 'view', $vote['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'votes', 'action' => 'edit', $vote['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'votes', 'action' => 'delete', $vote['id']), null, __('Are you sure you want to delete # %s?', $vote['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Vote'), array('controller' => 'votes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div> -->
