<?php echo $this->Form->create('Design',array('type'=>'file','class'=>'form-group')); ?>
	<legend><?php echo __('Add Design'); ?></legend>
	<h3>Info</h3>
	<?php
		if(isset($this->params['pass'][0])):
			$challengeName = $this->params['pass'][0];
		else:
			//$challengeName = 'evergreen';
			$challengeName = 1;
		endif;
		//echo $this->Form->input('challengeName', array('type'=>'hidden', 'value'=>$challengeName));
		echo $this->Form->input('challenge_id', array('type'=>'hidden', 'value'=>$challengeName));
		echo $this->Form->input('title',array('required'=>true, 'class'=>'form-control'));
		echo $this->Form->input('story',array('required'=>true, 'class'=>'form-control', 'type'=>'textarea', 'default'=>'Tell us your inspirations, where you where when you thought about the idea, why it\'s personal to you, your artistic statement etc'));
		echo $this->Form->input('notes',array('default'=>'Printing Notes such as colors etc', 'class'=>'form-control'));
		echo $this->Form->input('category_id',array('class'=>'form-control'));
		//echo $this->Form->input('designs_tags',array('class'=>'form-control'));
	?>
	<h3>Image</h3>
	Choose your image file to display your design. Your image must be in JPG format (RGB mode not CMYK), 1200 pixels wide x 1200 pixels tall.
	<?php echo $this->Form->input('image',array('type'=>'file')); ?>
	<br />
    <?php 
	echo $this->Form->input('legal', array('label'=>'I agree to the Legal Terms and understand my design can be dropped from scoring early if it gets a low score.', 'type'=>'checkbox', 'required'=>true));
    echo $this->Form->submit(__('Submit My Design',true), array('class'=>'btn btn-primary')); 
    echo $this->Form->end(); ?>

</div>