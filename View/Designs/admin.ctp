<div class="designs index">
	<h2><?php echo __('Designs'); ?></h2>
	<table class="table table-striped" cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('challenge_id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('story'); ?></th>
			<th><?php echo $this->Paginator->sort('notes'); ?></th>
			<th><?php echo $this->Paginator->sort('category_id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($designs as $design): ?>
	<tr>
		<td><?php echo h($design['Design']['id']); ?>&nbsp;</td>
		<td><?php echo $this->Html->image($design['Design']['image'], array('width'=>'70'));?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($design['Challenge']['name'], array('controller' => 'challenges', 'action' => 'view', $design['Challenge']['id'])); ?>
		</td>
		<td><?php echo h($design['Design']['title']); ?>&nbsp;</td>
		<td><?php echo h($design['Design']['story']); ?>&nbsp;</td>
		<td><?php echo h($design['Design']['notes']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($design['Category']['name'], array('controller' => 'categories', 'action' => 'view', $design['Category']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($design['User']['name'], array('controller' => 'users', 'action' => 'view', $design['User']['id'])); ?>
		</td>
		<td><?php echo h($design['Design']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $design['Design']['id']), array('class'=>'btn btn-info btn-rounded')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $design['Design']['id']), array('class'=>'btn btn-primary btn-rounded')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $design['Design']['id']), array('class'=>'btn btn-danger btn-rounded'), __('Are you sure you want to delete # %s?', $design['Design']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>