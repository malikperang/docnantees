<div class="challenges view">

<div class="row gray-bg">
<div class="row">
	<?php echo $this->Html->image($challenge['Challenge']['image'] , array('fullBase' => true ,'class' => 'course-img'));?>                   
</div>

<div class="row">
		<h3><?php echo h($challenge['Challenge']['header']); ?></h3>
</div>


<div class="row">
	<div class="col-md-6">
		<?php echo h($challenge['Challenge']['descriptions']); ?>
	</div>
	<div class="col-md-6">
		<h4>You could win</h4>
		<?php foreach ($challenge['Prize'] as $prize): ?>
			<li><?php echo $prize['name']; ?></li>
		<?php endforeach; ?>
		<div class="row">
			<?php echo $this->Html->link(__('Submit Design'), array('controller'=>'designs', 'action' => 'add', $challenge['Challenge']['id']), array('class'=>'btn btn-info btn-rounded')); ?>
		</div>
		<div class="row">
			<small>Submit from <?php echo h($challenge['Challenge']['startDate']); ?> - <?php echo h($challenge['Challenge']['endDate']); ?></small>
		</div>
	</div>
</div>
</div>

<div class="hr"></div>

<div class="row">
	<?php foreach ($challenge['Design'] as $design): ?>
		<?php echo '<div class="col-md-2">'; ?>
		<?php //echo $this->Html->image($design['image'] , array('fullBase' => true, 'width'=>'185', 'heigth'=>'185'));?>
        <?php //echo $this->Html->link(__('View'), array('controller' => 'designs', 'action' => 'view', $design['id'])); ?>
        <?php echo $this->Html->link($this->Html->image($design['image'], array('fullBase' => true, 'width'=>'185', 'heigth'=>'185')), array('controller' => 'designs', 'action' => 'view', $design['id']), array('escape' => false)); ?>
        <?php echo '</div>'; ?>
	<?php endforeach; ?>
</div>
