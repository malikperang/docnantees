<!-- <div class="challenges form"> -->
<?php echo $this->Form->create('Challenge', array('type'=>'file','class'=>'form-group')); ?>
	<fieldset>
		<legend><?php echo __('Add Challenge'); ?></legend>
	<?php
		echo $this->Form->input('name',array('class'=>'form-control'));
		echo $this->Form->input('descriptions', array('class'=>'form-control'));
		echo $this->Form->input('image', array('class'=>'form-control', 'type'=>'file'));
		echo $this->Form->input('header', array('class'=>'form-control'));
		echo $this->Form->input('shortcode', array('label'=>'Shortcode di url', 'class'=>'form-control', 'after'=>'** will be used at URL. Example: /challenges/shortcode/'));
		echo $this->Form->input('startDate');
		echo $this->Form->input('endDate');
		$options = array('ongoing'=>'ongoing', 'completed'=>'completed');
		echo $this->Form->input('status', array('type'=>'select', 'options'=>$options));
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Submit',true), array('class'=>'btn btn-primary')); echo $this->Form->end();?>    
</div>
<img src="<?php echo $this->webroot;?>img/challenges-descriptions.png"/>