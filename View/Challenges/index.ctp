<div class="quote-info">
<h1>DESIGN CHALLENGES</h1>
<p>This is where the magic happens. If you've got an awesome idea for a design, submit it here. If your design is chosen for print, it could end up for sale as a real product that people all around the world can have! </p>
</div>

<div class="contest gray-bg">
<h4 align="center">Join Our never-ending, no-themes, no-holds-barred, open-ended design challenge</h4>
<div class="col-lg-4">
<h3 align="center">20</h3>
<p align="center">Designs Submitted</p>
</div>
<div class="col-lg-4">
<?php echo $this->Html->link(__('Submit A Design'), array('controller'=>'designs', 'action' => 'add'), array('class'=>'btn btn-info btn-lg btn-block')); ?>
</div>
<div class="col-lg-4">
<h3 align="center">1040</h3>
<p align="center">Designs Printed</p>
</div>
</div>

<div class="hr"></div>

<div class="row content">
<h3>On going challenges</h3>
<?php foreach($ongoing as $current): ?>
<div class="row content">
	<div class="col-lg-3 portlet">
		<!-- <img align="center" width="195" src="<?php echo $this->webroot.$current['Challenge']['image']; ?>" /> -->
		<?php echo $this->Html->image($current['Challenge']['image'], array('align'=>'center', 'width'=>'195', 'heigth'=>'50'));?>
		<h3><p align="center"><?php echo $current['Challenge']['name']; ?></p></h3>
		<p align="center"><?php 
			foreach($current['Prize'] as $prize):
				echo ' + '.$prize['name'];
			endforeach;
			?>
		</p>
		<p align="center"><?php echo $this->Html->link(__('Submit a Design'), array('action' => 'view', $current['Challenge']['id']), array('class'=>'btn btn-primary')); ?></p>
	</div>
<?php endforeach; ?>
</div>
</div>

<div class="hr"></div>

<?php if(isset($completed)): ?>
<div class="row content">
<h3>Completed challenges</h3>
<?php foreach($completed as $current): ?>
<div class="row content">
	<div class="col-lg-2">
		<?php echo $this->Html->link($this->Html->image($current['Challenge']['image'], array('fullBase' => true, 'width'=>'185', 'heigth'=>'185')), array('controller' => 'challenges', 'action' => 'view', $current['Challenge']['id']), array('escape' => false)); ?>
	</div>
<?php endforeach; ?>
</div>
</div>
<?php endif; ?>

