<div class="challenges form">
<?php echo $this->Form->create('Challenge', array('type'=>'file','class'=>'form-group')); ?>
	<fieldset>
		<legend><?php echo __('Edit Challenge'); ?></legend>
	<?php
		echo $this->Form->input('id', array('class'=>'form-control'));
		echo $this->Form->input('name', array('class'=>'form-control'));
		echo $this->Form->input('descriptions', array('class'=>'form-control'));
		echo $this->Form->input('image', array('label'=>'Image Replacement', 'class'=>'form-control', 'type'=>'file'));
		echo $this->Form->input('header', array('class'=>'form-control'));
		echo $this->Form->input('startDate');
		echo $this->Form->input('endDate');
		echo $this->Form->input('shortcode', array('class'=>'form-control'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
