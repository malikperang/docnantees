<div class="challenges index">
	<p>
	<h2><?php echo __('Challenges'); ?></h2>
	<?php echo $this->Html->link(__('Add New Challenges'), array('action' => 'add'), array('class'=>'btn btn-info btn-rounded')); ?>
	</p>
	<table class="table table-striped" cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('header'); ?></th>
			<th><?php echo $this->Paginator->sort('startDate'); ?></th>
			<th><?php echo $this->Paginator->sort('endDate'); ?></th>
			<th><?php echo $this->Paginator->sort('shortcode'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($challenges as $challenge): ?>
	<tr>
		<td><?php echo h($challenge['Challenge']['id']); ?>&nbsp;</td>
		<td><?php echo h($challenge['Challenge']['name']); ?>&nbsp;</td>
		<td><?php echo h($challenge['Challenge']['descriptions']); ?>&nbsp;</td>
		<td><?php echo $this->Html->image($challenge['Challenge']['image'], array('width'=>'70'));?></td>
		<td><?php echo h($challenge['Challenge']['header']); ?>&nbsp;</td>
		<td><?php echo h($challenge['Challenge']['startDate']); ?>&nbsp;</td>
		<td><?php echo h($challenge['Challenge']['endDate']); ?>&nbsp;</td>
		<td><?php echo h($challenge['Challenge']['shortcode']); ?>&nbsp;</td>
		<td><?php echo h($challenge['Challenge']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $challenge['Challenge']['id']), array('class'=>'btn btn-info btn-rounded')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $challenge['Challenge']['id']), array('class'=>'btn btn-primary btn-rounded')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $challenge['Challenge']['id']), array('class'=> 'btn btn-danger'), __('Are you sure you want to delete # %s?', $challenge['Challenge']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>