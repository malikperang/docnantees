<div class="categories index">
<?php 
echo $this->Html->css('tree'); 
?>
<style>
    #content ol li input:checked + ol > li {
    display: block;
    margin: 0 0 0.125em;
    }
</style>


<h2><?php echo __('Categories'); ?></h2>

<div class="col-md-6">
	<div id="tree" class="demo">
        <ol class="tree">
            <?php
                echo createListTree($treeLists);
            ?>
        </ol>
    </div>	
</div>

<div class="col-md-4">
	<div class="categories form">
    <?php echo $this->Form->create('Category', array('type' => 'file'));?>
    <fieldset>
    	<legend><?php echo __('Add Category'); ?></legend>
    <?php
        echo $this->Form->input('parent_id', array('class'=>'form-control', 'label' => 'Parent Category'));
        echo $this->Form->input('name', array('class'=>'form-control'));
        echo $this->Form->input('description', array('class'=>'form-control'));
    ?>
    </fieldset>
    <?php echo $this->Form->submit(__('Submit',true), array('class'=>'btn btn-primary')); echo $this->Form->end();?>
    </div>
</div>


<?php
    
    function createListTree($tree) {
        
        $out = '';
    
        $depth = 0;
        $prev_depth = 0;
        $count = 0;
        foreach ($tree as $id => $node) {
            $depth = strrpos($node, '_');
            if ($depth === false) {
                $depth = 0;
                $clean_node = $node;
            } else {
                $depth = $depth + 1;
                $clean_node = substr($node, strrpos($node, '_')+1);
            }

            if ($depth > $prev_depth) {
                $out .= "\n<ol>\n";
            } else if ($depth < $prev_depth) {
                for ($i = 0; $i < ($prev_depth-$depth); $i++) {
                    $out .= "</li></ol>\n";
                }
            } else if ($count>0) {
                $out .= "\n</li>\n";
            }
            
            $out .= '<li id="node_' . $id . '"><label for="folder1">' . $clean_node . ' <span style="font-size: 12px"><a class="btn btn-primary" href="./categories/edit/'.$id.'">Edit</a>';
            $out .= '<a class="btn btn-danger" href="./categories/delete/'.$id.'">Delete</a></span></label> ';
            $out .= "\n";

            $prev_depth = $depth;
            $count++;
        }
        for ($i = 0; $i < ($depth); $i++) {
            $out .= "</li></ol>\n";
        }
        if (!empty($tree)) {
            $out .= '</li>';
        }

        return $out;
    }
  ?>  

<!-- 

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('parent_id'); ?></th>
			<th><?php echo $this->Paginator->sort('lft'); ?></th>
			<th><?php echo $this->Paginator->sort('rght'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($categories as $category): ?>
	<tr>
		<td><?php echo h($category['Category']['id']); ?>&nbsp;</td>
		<td><?php echo h($category['Category']['name']); ?>&nbsp;</td>
		<td><?php echo h($category['Category']['description']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($category['ParentCategory']['name'], array('controller' => 'categories', 'action' => 'view', $category['ParentCategory']['id'])); ?>
		</td>
		<td><?php echo h($category['Category']['lft']); ?>&nbsp;</td>
		<td><?php echo h($category['Category']['rght']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $category['Category']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $category['Category']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $category['Category']['id']), null, __('Are you sure you want to delete # %s?', $category['Category']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Category'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Designs'), array('controller' => 'designs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Design'), array('controller' => 'designs', 'action' => 'add')); ?> </li>
	</ul>
</div> -->
</div>
