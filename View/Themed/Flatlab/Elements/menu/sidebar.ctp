<?php
  $challenges = $this->requestAction('challenges/getList/sort:created/direction:asc/limit:20');
  $categories = $this->requestAction('categories/getList/sort:created/direction:asc/limit:20');
?>
<div class="col-xs-7 col-lg-2" style="border:1px solid #ddd;border-radius:3px;">
        <div class="blog-side-item">
            <div class="category">
                <h3>Challenges</h3>
                <ul class="list-unstyled">
                    <?php foreach($challenges as $challenge): ?>
                    <?php //debug($challenge); ?>
                    <li><?php echo $this->Html->link(__($challenge['Challenge']['name']), array('controller'=>'designs', 'action' => 'challenge', $challenge['Challenge']['shortcode'])); ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
    </div>
    <div class="blog-side-item">
            <div class="category">
                <h3>Categories</h3>
                <ul class="list-unstyled">
                <?php foreach($categories as $category): ?>
                    <?php //debug($category); ?>
                    <li><?php echo $this->Html->link(__($category['Category']['name']), array('controller'=>'designs', 'action' => 'index', $category['Category']['shortcode'])); ?></li>
                <?php endforeach; ?>
                </ul>
            </div>
    </div>
</div>
        <!--sidebar menu end-->