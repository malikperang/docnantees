<?php $userDetails = $this->Session->read('Auth.User');?>

<?php $current_page = $this->params['action']; ?>
<div class="navbar-collapse collapse ">
    <ul class="nav navbar-nav">
       	<li class="active"><?php echo $this->Html->link(__('Home'), array('plugin'=>false, 'controller'=>'pages','action' => 'home')); ?></li>
       	<!-- <li><?php echo $this->Html->link(__('Design Challenges'), array('plugin'=>false,'controller'=>'challenges','action' => 'index')); ?></li> -->
       	<li class="dropdown">
        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Design Challenges<b class="caret"></b></a>
        	<ul class="dropdown-menu">
        	    <li><?php echo $this->Html->link(__('Challenges'), array('plugin'=>false,'admin'=>false, 'controller'=>'challenges','action' => 'index')); ?></li>
				<li><?php echo $this->Html->link(__('Designs'), array('plugin'=>false, 'admin'=>false, 'controller'=>'designs','action' => 'index')); ?></li>
                <?php if(!isset($userDetails)):?>
				<li><?php echo $this->Html->link(__('Sign Up'), array('plugin'=>false, 'admin'=>false, 'controller'=>'users','action' => 'register')); ?></li>
				<li><?php echo $this->Html->link(__('Login'), array('plugin'=>false, 'admin'=>false, 'controller'=>'users','action' => 'login')); ?></li>
                <?php else: ?>
                    <li><?php echo $this->Html->link(__('Log out'), array('plugin'=>false, 'controller'=>'users','action' => 'logout')); ?></li>               
                <?php endif; ?>

        	</ul>
        </li>
        <!-- <li><a href="../dropship/stores">Tees Store</a></li>
        <li><a href="../dropship/stores/join_us">Join Dropship</a></li> -->
        <li><a href="http://dropship.demodocnantees.com/stores">Tees Store</a></li>
        <li><a href="http://dropship.demodocnantees.com/stores/join_us">Join Dropship</a></li>
        <!-- <li class="dropdown">
        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Vote Ranking<b class="caret"></b></a>
        	<ul class="dropdown-menu">
        	    <li><?php echo $this->Html->link(__('All Time!'), array('plugin'=>false,'controller'=>'votes','action' => 'vote_index')); ?></li>
				<li><?php echo $this->Html->link(__('This Month! '), array('plugin'=>false,'controller'=>'votes','action' => 'monthly_vote')); ?></li>
        		<li><?php echo $this->Html->link(__('This Week!'), array('plugin'=>false,'controller'=>'votes','action' => 'weekly_vote')); ?></li>
        		<li><?php echo $this->Html->link(__('Today!'), array('plugin'=>false,'controller'=>'votes','action' => 'daily_vote')); ?></li> 
        	</ul>
        </li> -->
        <?php if($userDetails['group_id'] == 1):?>
        <li class="dropdown">
        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin<b class="caret"></b></a>
        	<ul class="dropdown-menu">
        	    <li><?php echo $this->Html->link(__('Challenges Mgmt'), array('plugin'=>false,'admin'=>false,'controller'=>'challenges','action' => 'admin')); ?></li>
                <li><?php echo $this->Html->link(__('Designs Mgmt'), array('plugin'=>false, 'controller'=>'designs','action' => 'admin')); ?></li>
                <li><?php echo $this->Html->link(__('Users'), array('plugin'=>false, 'controller'=>'users','action' => 'index')); ?></li>
				<li><?php echo $this->Html->link(__('Banners'), array('plugin'=>false, 'controller'=>'banners','action' => 'index')); ?></li>
        		<li><?php echo $this->Html->link(__('Categories'), array('plugin'=>false, 'controller'=>'categories','action' => 'index')); ?></li>
        		<li><?php echo $this->Html->link(__('Vote'), array('plugin'=>false, 'controller'=>'votes','action' => 'index')); ?></li>        
        		<li><?php echo $this->Html->link(__('Log out'), array('plugin'=>false, 'controller'=>'users','action' => 'logout')); ?></li>
        	</ul>
        </li>
        <?php endif;?>				
    </ul>
</div>
            
<!-- <div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" target="_blank" href="http://twitter.github.com/bootstrap/">Bootstrap</a>
			<div class="nav-collapse">
				<ul class="nav">
					<li <?php if($current_page=="index"){echo'class="active"';} ?>>
						<?php echo $this->Html->link('Scaffolding', array('controller' => 'app', 'action' => 'index')); ?>
					</li>
					<li <?php if($current_page=="base_css"){echo'class="active"';} ?>>
						<?php echo $this->Html->link('Base CSS', array('controller' => 'app', 'action' => 'base_css')); ?>
					</li>
					<li <?php if($current_page=="components"){echo'class="active"';} ?>>
						<?php echo $this->Html->link('Components', array('controller' => 'app', 'action' => 'components')); ?>
					</li>
					<li <?php if($current_page=="javascript"){echo'class="active"';} ?>>
						<?php echo $this->Html->link('Javascript plugins', array('controller' => 'app', 'action' => 'javascript')); ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div> -->