<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'Docnantees');
?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta(array("name"=>"viewport","content"=>"width=device-width,  initial-scale=1.0"));
		echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('theme');
		echo $this->Html->css('bootstrap-reset');
		echo $this->Html->css('bootstrap-reset');
		echo $this->Html->css('flexslider.css');
		echo $this->Html->css('jquery.bxslider.css');
		echo $this->Html->css('jquery.fancybox');
		echo $this->Html->css('rs-style');
		echo $this->Html->css('settings');
		echo $this->Html->css('style');
		echo $this->Html->css('style-responsive.css');		

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->Html->script('jquery');
		echo $this->Html->script('modernizr.min');
		echo $this->Html->script('jquery');
		echo $this->Html->script('bootstrap.min');
		echo $this->fetch('script');
	?>

</head>
<body>
	<!--header start-->
    <header class="header-frontend">
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="fa fa-bar"></span>
                        <span class="fa fa-bar"></span>
                        <span class="fa fa-bar"></span>
                    </button>
                    <?php echo $this->Html->link($this->Html->image('docnantees-logo.png'), array('controller' => 'challenges', 'action' => 'index'), array('class'=>'navbar-brand', 'escape' => false)); ?>
                </div>
                <?php echo $this->element('menu/top_menu'); ?>
            </div>
        </div>
    </header>
    <!--header end-->

	<div class="container">
		<div class="row">
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>

			<?php echo $this->element('sql_dump'); ?>
		</div>
	</div>

	<!--footer start-->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-3">
                    <h1>contact info</h1>
                    <address>
                        <p>Docnan Industries Sdn. Bhd.</p> 
                        <p>352, Simpang Tiga Tanjong Mas,</p>
                        <p>Jalan Pengkalan Chepa</p>
                        <p>15400 Kota Bharu,</p> 
                        <p>Kelantan</p>
                        
                        <p>Phone : 011 1086 3966 / 011 1089 3966</p>
                        <p>Fax : (60) 09-7715889</p>
                        <p>Email : docnantees@gmail.com</p>
                    </address>
                </div>
                <div class="col-lg-5 col-sm-5">
                    <h1>latest tweet</h1>
                    <div class="tweet-box">
                        <i class="fa fa-twitter"></i>
                        <em>Please follow <a href="javascript:;">@nettus</a> for all future updates of us! <a href="javascript:;">twitter.com/vectorlab</a></em>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-lg-offset-1">
                    <h1>stay connected</h1>
                    <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2FDocnanTees%2F&amp;width=220&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=461732827261378" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:220px; height:80px;" allowTransparency="true"></iframe>
                </div>
            </div>
        </div>
    </footer>
    <!--footer end-->

	<?php //echo $this->element('sql_dump'); ?>

	<?php 
		echo $this->Html->script('jquery');
		echo $this->Html->script('jquery-1.8.3.min');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('hover-dropdown');
		echo $this->Html->script('jquery.flexslider');
		echo $this->Html->script('jquery.bxslider');
		echo $this->Html->script('jquery.parallax-1.1.3');
		echo $this->Html->script('jquery.easing.min');
		echo $this->Html->script('link-hover');
		echo $this->Html->script('jquery.fancybox.pack');
		echo $this->Html->script('themepunch.plugins.min');
		echo $this->Html->script('themepunch.revolution.min');
		echo $this->Html->script('common-scripts');
		echo $this->Html->script('revulation-slide');
	?>

	<script>

      /*RevSlide.initRevolutionSlider();

      $(window).load(function() {
          $('[data-zlname = reverse-effect]').mateHover({
              position: 'y-reverse',
              overlayStyle: 'rolling',
              overlayBg: '#fff',
              overlayOpacity: 0.7,
              overlayEasing: 'easeOutCirc',
              rollingPosition: 'top',
              popupEasing: 'easeOutBack',
              popup2Easing: 'easeOutBack'
          });
      });

      $(window).load(function() {
          $('.flexslider').flexslider({
              animation: "slide",
              start: function(slider) {
                  $('body').removeClass('loading');
              }
          });
      });

      //    fancybox
      jQuery(".fancybox").fancybox();

  </script>

</body>
</html>
