-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 21, 2014 at 03:14 AM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nucleus_docnantees`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 126),
(9, 1, NULL, NULL, 'AclManagement', 2, 37),
(10, 9, NULL, NULL, 'Groups', 3, 14),
(11, 10, NULL, NULL, 'index', 4, 5),
(12, 10, NULL, NULL, 'view', 6, 7),
(13, 10, NULL, NULL, 'add', 8, 9),
(14, 10, NULL, NULL, 'edit', 10, 11),
(15, 10, NULL, NULL, 'delete', 12, 13),
(16, 9, NULL, NULL, 'UserPermissions', 15, 24),
(17, 16, NULL, NULL, 'index', 16, 17),
(18, 16, NULL, NULL, 'sync', 18, 19),
(19, 16, NULL, NULL, 'edit', 20, 21),
(20, 16, NULL, NULL, 'toggle', 22, 23),
(21, 9, NULL, NULL, 'Users', 25, 36),
(24, 21, NULL, NULL, 'index', 26, 27),
(25, 21, NULL, NULL, 'view', 28, 29),
(26, 21, NULL, NULL, 'add', 30, 31),
(27, 21, NULL, NULL, 'edit', 32, 33),
(28, 21, NULL, NULL, 'delete', 34, 35),
(30, 1, NULL, NULL, 'Banners', 38, 49),
(31, 30, NULL, NULL, 'index', 39, 40),
(32, 30, NULL, NULL, 'view', 41, 42),
(33, 30, NULL, NULL, 'add', 43, 44),
(34, 30, NULL, NULL, 'edit', 45, 46),
(35, 30, NULL, NULL, 'delete', 47, 48),
(36, 1, NULL, NULL, 'Categories', 50, 61),
(37, 36, NULL, NULL, 'index', 51, 52),
(38, 36, NULL, NULL, 'view', 53, 54),
(39, 36, NULL, NULL, 'add', 55, 56),
(40, 36, NULL, NULL, 'edit', 57, 58),
(41, 36, NULL, NULL, 'delete', 59, 60),
(42, 1, NULL, NULL, 'Designs', 62, 73),
(43, 42, NULL, NULL, 'index', 63, 64),
(44, 42, NULL, NULL, 'view', 65, 66),
(45, 42, NULL, NULL, 'add', 67, 68),
(46, 42, NULL, NULL, 'edit', 69, 70),
(47, 42, NULL, NULL, 'delete', 71, 72),
(48, 1, NULL, NULL, 'Pages', 74, 77),
(49, 48, NULL, NULL, 'display', 75, 76),
(50, 1, NULL, NULL, 'Tags', 78, 89),
(51, 50, NULL, NULL, 'index', 79, 80),
(52, 50, NULL, NULL, 'view', 81, 82),
(53, 50, NULL, NULL, 'add', 83, 84),
(54, 50, NULL, NULL, 'edit', 85, 86),
(55, 50, NULL, NULL, 'delete', 87, 88),
(56, 1, NULL, NULL, 'Themes', 90, 101),
(57, 56, NULL, NULL, 'index', 91, 92),
(58, 56, NULL, NULL, 'view', 93, 94),
(59, 56, NULL, NULL, 'add', 95, 96),
(60, 56, NULL, NULL, 'edit', 97, 98),
(61, 56, NULL, NULL, 'delete', 99, 100),
(62, 1, NULL, NULL, 'Users', 102, 113),
(63, 62, NULL, NULL, 'index', 103, 104),
(64, 62, NULL, NULL, 'view', 105, 106),
(65, 62, NULL, NULL, 'add', 107, 108),
(66, 62, NULL, NULL, 'edit', 109, 110),
(67, 62, NULL, NULL, 'delete', 111, 112),
(68, 1, NULL, NULL, 'Votes', 114, 125),
(69, 68, NULL, NULL, 'index', 115, 116),
(70, 68, NULL, NULL, 'view', 117, 118),
(71, 68, NULL, NULL, 'add', 119, 120),
(72, 68, NULL, NULL, 'edit', 121, 122),
(73, 68, NULL, NULL, 'delete', 123, 124);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, NULL, 1, 4),
(2, NULL, 'Group', 2, NULL, 5, 10),
(3, 1, 'User', 1, NULL, 2, 3),
(4, 2, 'User', 2, NULL, 6, 7),
(5, 2, 'User', 3, NULL, 8, 9);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 1, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `image_url` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `modified_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `banners`
--


-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `right` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `categories`
--


-- --------------------------------------------------------

--
-- Table structure for table `designs`
--

CREATE TABLE IF NOT EXISTS `designs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `story` varchar(100) NOT NULL,
  `notes` varchar(100) NOT NULL,
  `catogory_id` int(11) NOT NULL,
  `designs_tags` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `designs`
--


-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Admin', '2012-01-15 16:16:09', '2012-01-15 16:16:09'),
(2, 'Member', '2012-01-15 16:16:16', '2012-01-15 16:16:16');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created`, `modified`) VALUES
(1, 'The title', 'This is the post body.', '2012-01-15 16:20:44', NULL),
(2, 'A title once again', 'And the post body follows.', '2012-01-15 16:20:44', NULL),
(3, 'Title strikes back', 'This is really exciting! Not.', '2012-01-15 16:20:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tags`
--


-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `themes`
--


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` tinytext COLLATE utf8_unicode_ci COMMENT 'full url to avatar image file',
  `language` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `group_id`, `username`, `name`, `password`, `email`, `avatar`, `language`, `timezone`, `token`, `status`, `created`, `modified`, `last_login`) VALUES
(1, 1, NULL, 'Admin', 'ab6ca9fb7e2731ef145b21f6a0c2bcb9efe1b5e9', 'admin@nucleus.com.my', NULL, NULL, NULL, '4f129978-fd3c-445e-8239-2feb2b2926db', 1, '2012-01-15 16:16:40', '2012-01-15 16:16:40', '0000-00-00 00:00:00'),
(2, 2, NULL, 'Member', '12da3e452ea3a4ba9bb340a24e4e2c15232a7d42', 'member@dev.com', NULL, NULL, NULL, '4f12998d-a7cc-4b64-8d13-30292b2926db', 1, '2012-01-15 16:17:01', '2012-01-15 16:17:01', '0000-00-00 00:00:00'),
(3, 1, NULL, 'Ramzan', 'ab6ca9fb7e2731ef145b21f6a0c2bcb9efe1b5e9', 'ramzan.rozali@gmail.com', NULL, NULL, NULL, '336d184d485b0fdce6cb9fa732368099', 1, '2014-01-21 03:07:02', '2014-01-21 03:07:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `design_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `votes`
--

