<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */


class AppController extends Controller {

    public $theme = "Flatlab";
    
    public $helpers = array(
        'Facebook.Facebook' => array(
            'appId' => '129351393906701',
            'appSecret' => '667ca61dd3d74c3cfc37619fada8fc97'
        )
    );
    
	public $components = array(
        'Acl',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email'),
                    'scope'  => array('User.status' => 1),
                    //'Controller'
                )
            ),
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers'),
                //'Controller'
            )
        ),
        'Session',
        'Facebook.Facebook' => array(
        'appId' => '129351393906701',
        'appSecret' => '667ca61dd3d74c3cfc37619fada8fc97'
        )
    );

    public function beforeFilter() {
      parent::beforeFilter();
    
     //Configure AuthComponent
     $this->Auth->loginAction = '/users/login';
     $this->Auth->logoutRedirect = '/users/login';
     $this->Auth->loginRedirect = array('plugin'=>false, 'controller' => 'designs', 'action' => 'index');

    }
}
