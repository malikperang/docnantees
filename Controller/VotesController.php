<?php
App::uses('AppController', 'Controller');
/**
 * Votes Controller
 *
 * @property Vote $Vote
 * @property PaginatorComponent $Paginator
 */
class VotesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Vote->recursive = 0;
		$this->set('votes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Vote->exists($id)) {
			throw new NotFoundException(__('Invalid vote'));
		}
		$options = array('conditions' => array('Vote.' . $this->Vote->primaryKey => $id));
		$this->set('vote', $this->Vote->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$design_id = $this->Session->read('Design.id');
			$this->Vote->create();
			//[debugging] what data has been requested
			//debug($this->request->data);

			//voting method start
			if (isset($this->request->data['one'])) {
				$data = array('user_id' => $this->Auth->user('id'),
							  'design_id' => $design_id,
					   		  'rating' => 1);
				$this->Vote->save($data);
				//redirect if success
				return $this->redirect(array('controller'=> 'designs' ,'action' => 'index'));
			   
			} else if (isset($this->request->data['two'])) {
				$data = array('user_id' => $this->Auth->user('id'),
							  'design_id' => $design_id,
					   		  'rating' => 2);
				$this->Vote->save($data);
				//redirect if success
				$this->redirect(array('controller'=> 'designs' ,'action' => 'index'));
			} else if (isset($this->request->data['three'])){
				$data = array('user_id' => $this->Auth->user('id'),
							  'design_id' => $design_id,
					   		  'rating' => 3);
				$this->Vote->save($data);
				//redirect if success
				$this->redirect(array('controller'=> 'designs' ,'action' => 'index'));

			} else if (isset($this->request->data['four'])){
				$data = array('user_id' => $this->Auth->user('id'),
							  'design_id' => $design_id,
					   		  'rating' => 4);
				$this->Vote->save($data);
				//redirect if success
				$this->redirect(array('controller'=> 'designs' ,'action' => 'index'));

			} else if (isset($this->request->data['five'])){
				$data = array('user_id' => $this->Auth->user('id'),
							  'design_id' => $design_id,
					   		  'rating' => 5);
				$this->Vote->save($data);
				//redirect if success
				$this->redirect(array('controller'=> 'designs' ,'action' => 'index'));

			} 
			else {
				$this->Session->setFlash(__('Error on voting!'));
				$this->redirect(array('controller'=> 'designs' ,'action' => 'index'));
			}
		}
		/*below uncommented are the old code*/
		//$users = $this->Vote->User->find('list');
		//$designs = $this->Vote->Design->find('list');
		//$this->set(compact('users', 'designs'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Vote->exists($id)) {
			throw new NotFoundException(__('Invalid vote'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Vote->save($this->request->data)) {
				$this->Session->setFlash(__('The vote has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vote could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Vote.' . $this->Vote->primaryKey => $id));
			$this->request->data = $this->Vote->find('first', $options);
		}
		$users = $this->Vote->User->find('list');
		$designs = $this->Vote->Design->find('list');
		$this->set(compact('users', 'designs'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Vote->id = $id;
		if (!$this->Vote->exists()) {
			throw new NotFoundException(__('Invalid vote'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Vote->delete()) {
			$this->Session->setFlash(__('The vote has been deleted.'));
		} else {
			$this->Session->setFlash(__('The vote could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	//redirect function after success voting
	public function success(){
		$this->render();
	}

	//
	public function vote_index(){
		$topvoterank =  $this->Vote->query("SELECT tvotes.design_id,SUM(tvotes.rating)AS totalrate,dd.title AS name,tvotes.created FROM votes tvotes LEFT JOIN designs dd ON dd.id = tvotes.design_id GROUP BY tvotes.design_id ORDER BY totalrate DESC ");
		//$topvoterank = $this->Paginator->paginate($topvoterank);
		$this->set('topvoterank',$topvoterank);
	}

	//monthly top votes
	public function monthly_vote(){
		$monthlyvote = $this->Vote->query("SELECT tvotes.design_id,SUM(tvotes.rating) AS totalrate,dd.title AS name,tvotes.created 
											FROM votes tvotes 
											LEFT JOIN designs dd ON dd.id = tvotes.design_id
											WHERE tvotes.created 
											BETWEEN DATE_SUB(NOW(),INTERVAL 1 MONTH) AND NOW()
											GROUP BY tvotes.design_id ORDER BY totalrate DESC");
		$this->set('monthlyvote',$monthlyvote);
	} 

	//weekly top votes
	public function weekly_vote(){
		$weeklyvote = $this->Vote->query("SELECT tvotes.design_id,SUM(tvotes.rating) AS totalrate,dd.title AS name,tvotes.created 
										  FROM votes tvotes 
										  LEFT JOIN designs dd ON dd.id = tvotes.design_id
										  WHERE tvotes.created 
										  BETWEEN DATE_SUB(NOW(),INTERVAL 1 WEEK) AND NOW()
										  GROUP BY tvotes.design_id ORDER BY totalrate DESC
											");
		$this->set('weeklyvote',$weeklyvote);
	}

	//daily votes
	public function daily_vote(){
		$dailyvote = $this->Vote->query("SELECT tvotes.design_id,SUM(tvotes.rating) AS totalrate,dd.title AS name,tvotes.created 
										 FROM votes tvotes 
										 LEFT JOIN designs dd ON dd.id = tvotes.design_id
										 WHERE tvotes.created 
										 BETWEEN DATE_SUB(NOW(),INTERVAL 1 DAY) AND NOW()
										 GROUP BY tvotes.design_id ORDER BY totalrate DESC");
		$this->set('dailyvote',$dailyvote);
	}
}
