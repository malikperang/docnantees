<?php
App::uses('AppController', 'Controller');
/**
 * Prizes Controller
 *
 * @property Prize $Prize
 * @property PaginatorComponent $Paginator
 */
class PrizesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Prize->recursive = 0;
		$this->set('prizes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Prize->exists($id)) {
			throw new NotFoundException(__('Invalid prize'));
		}
		$options = array('conditions' => array('Prize.' . $this->Prize->primaryKey => $id));
		$this->set('prize', $this->Prize->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Prize->create();
			if ($this->Prize->save($this->request->data)) {
				$this->Session->setFlash(__('The prize has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prize could not be saved. Please, try again.'));
			}
		}
		$challenges = $this->Prize->Challenge->find('list');
		$this->set(compact('challenges'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Prize->exists($id)) {
			throw new NotFoundException(__('Invalid prize'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Prize->save($this->request->data)) {
				$this->Session->setFlash(__('The prize has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The prize could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Prize.' . $this->Prize->primaryKey => $id));
			$this->request->data = $this->Prize->find('first', $options);
		}
		$challenges = $this->Prize->Challenge->find('list');
		$this->set(compact('challenges'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Prize->id = $id;
		if (!$this->Prize->exists()) {
			throw new NotFoundException(__('Invalid prize'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Prize->delete()) {
			$this->Session->setFlash(__('The prize has been deleted.'));
		} else {
			$this->Session->setFlash(__('The prize could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
