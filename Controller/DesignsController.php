<?php
App::uses('AppController', 'Controller');
/**
 * Designs Controller
 *
 * @property Design $Design
 * @property PaginatorComponent $Paginator
 */
class DesignsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	// public function beforeFilter() {
 //        parent::beforeFilter();
 //        $this->Auth->allow('index', 'view');
 //    }
/**
 * index method
 *
 * @return void
 */
	public function index($name = null) {
		
		$this->Design->recursive = 0;

		if(isset($name)):
			$this->Paginator->settings = array(
	        'conditions' => array('Category.shortcode' => $name),
	        'limit' => 100
    		);
		endif;

		$this->set('designs', $this->Paginator->paginate());
	}

	public function challenge($name = null) {
		
		$this->Design->recursive = 0;

		if(isset($name)):
			$this->Paginator->settings = array(
	        'conditions' => array('Challenge.shortcode' => $name),
	        'limit' => 100
    		);
		endif;

		$this->set('designs', $this->Paginator->paginate());
	}

	public function admin() {
		$this->Design->recursive = 0;
		$designs = $this->Paginator->paginate();
		$this->set('designs', $designs);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Design->exists($id)) {
			throw new NotFoundException(__('Invalid design'));
		}
		$options = array('conditions' => array('Design.' . $this->Design->primaryKey => $id));
		$this->set('design', $this->Design->find('first', $options));
		$this->Session->write('Design.id',$id);
	}

	public function admin_view($id = null) {
		if (!$this->Design->exists($id)) {
			throw new NotFoundException(__('Invalid design'));
		}
		$options = array('conditions' => array('Design.' . $this->Design->primaryKey => $id));
		$this->set('design', $this->Design->find('first', $options));
		$this->Session->write('Design.id',$id);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		//debug($this->data);
		if ($this->request->is('post')) {
			if ($this->Design->save($this->request->data)) {
				$this->Session->setFlash(__('The design has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The design could not be saved. Please, try again.'));
			}
		}
		$challenge = $this->Design->Challenge->find('list');

		$parents[0] = "[Top]";
        $categories = $this->Design->Category->generateTreeList(null,null,null," - ");
        if($categories) {
        foreach ($categories as $key=>$value)
        $parents[$key] = $value;
        $treeLists[$key] = $value;
        }

		$this->set(compact('challenge', 'categories'));

	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Design->create();
			if ($this->Design->save($this->request->data)) {
				$this->Session->setFlash(__('The design has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The design could not be saved. Please, try again.'));
			}
		}
		$challenge = $this->Design->Challenge->find('list');

		$parents[0] = "[Top]";
        $categories = $this->Design->Category->generateTreeList(null,null,null," - ");
        if($categories) {
        foreach ($categories as $key=>$value)
        $parents[$key] = $value;
        $treeLists[$key] = $value;
        }

		$this->set(compact('challenge', 'categories'));

	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Design->exists($id)) {
			throw new NotFoundException(__('Invalid design'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Design->save($this->request->data)) {
				$this->Session->setFlash(__('The design has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The design could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Design.' . $this->Design->primaryKey => $id));
			$this->request->data = $this->Design->find('first', $options);
		}
		$challenges = $this->Design->Challenge->find('list');
		$categories = $this->Design->Category->find('list');
		$this->set(compact('challenges', 'categories'));
	}


	public function admin_edit($id = null) {
		if (!$this->Design->exists($id)) {
			throw new NotFoundException(__('Invalid design'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Design->save($this->request->data)) {
				$this->Session->setFlash(__('The design has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The design could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Design.' . $this->Design->primaryKey => $id));
			$this->request->data = $this->Design->find('first', $options);
		}
		$categories = $this->Design->Category->find('list');
		$this->set(compact('themes', 'categories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Design->id = $id;
		if (!$this->Design->exists()) {
			throw new NotFoundException(__('Invalid design'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Design->delete()) {
			$this->Session->setFlash(__('The design has been deleted.'));
		} else {
			$this->Session->setFlash(__('The design could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_delete($id = null) {
		$this->Design->id = $id;
		if (!$this->Design->exists()) {
			throw new NotFoundException(__('Invalid design'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Design->delete()) {
			$this->Session->setFlash(__('The design has been deleted.'));
		} else {
			$this->Session->setFlash(__('The design could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


}
