<?php
App::uses('AppModel', 'Model');
/**
 * Prize Model
 *
 * @property Challenge $Challenge
 */
class Prize extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Challenge' => array(
			'className' => 'Challenge',
			'foreignKey' => 'challenge_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
