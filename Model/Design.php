<?php
App::uses('AppModel', 'Model');
/**
 * Design Model
 *
 * @property Theme $Theme
 * @property Category $Category
 * @property Vote $Vote
 */
class Design extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	// public $validate = array(
	// 	'theme_id' => array(
	// 		'numeric' => array(
	// 			//'rule' => array('numeric'),
	// 			//'message' => 'Your custom message here',
	// 			'allowEmpty' => true,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 	'title' => array(
	// 		'notEmpty' => array(
	// 			'rule' => array('notEmpty'),
	// 			//'message' => 'Your custom message here',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 	'story' => array(
	// 		'notEmpty' => array(
	// 			'rule' => array('notEmpty'),
	// 			//'message' => 'Your custom message here',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 	'notes' => array(
	// 		'notEmpty' => array(
	// 			'rule' => array('notEmpty'),
	// 			//'message' => 'Your custom message here',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 	'category_id' => array(
	// 		'numeric' => array(
	// 			'rule' => array('numeric'),
	// 			//'message' => 'Your custom message here',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 	'designs_tags' => array(
	// 		'numeric' => array(
	// 			'rule' => array('numeric'),
	// 			//'message' => 'Your custom message here',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// 	'image' => array(
	// 		'notEmpty' => array(
	// 			//'rule' => array('notEmpty'),
	// 			//'message' => 'Your custom message here',
	// 			//'allowEmpty' => false,
	// 			//'required' => false,
	// 			//'last' => false, // Stop validation after this rule
	// 			//'on' => 'create', // Limit validation to 'create' or 'update' operations
	// 		),
	// 	),
	// );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Challenge' => array(
			'className' => 'Challenge',
			'foreignKey' => 'challenge_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Vote' => array(
			'className' => 'Vote',
			'foreignKey' => 'design_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

/**
 *
 * Upload function & rules.
 * 
 * @var array
 */

	public $actsAs = array(
	'Containable',
	'Uploader.Attachment' => array(
		// Do not copy all these settings, it's merely an example
		'image' => array(
			'maxWidth' => 1200,
			'maxHeight' => 1200,
			'extension' => array('gif', 'jpg', 'png', 'jpeg'),
			'nameCallback' => '',
			'append' => '',
			'prepend' => '',
			//'tempDir' => TMP,
			'uploadDir' => '',
			'transportDir' => '',
			'finalPath' => '',
			'dbColumn' => '',
			'metaColumns' => array(),
			'defaultPath' => '',
			'overwrite' => true,
			'transforms' => array(),
			'stopSave' => true,
			'allowEmpty' => true,
			'transformers' => array(),
			'transport' => array(),
			'transporters' => array(),
			'curl' => array()
		)
	)
	);

}
